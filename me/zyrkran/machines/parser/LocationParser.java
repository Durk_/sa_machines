package me.zyrkran.machines.parser;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocationParser {
	
	private Location location;
	
	public LocationParser() { }
	
	public LocationParser(Location location){
		this.location = location;
	}
	
	public String parse(){
		return location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getZ();
	}
	
	public Location parseString(String string){
		String[] args = string.split(";");
		return new Location(Bukkit.getWorld(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]));
	}

}
