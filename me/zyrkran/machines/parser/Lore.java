package me.zyrkran.machines.parser;

import java.util.ArrayList;
import java.util.List;

import net.md_5.bungee.api.ChatColor;

public class Lore {
	
	private List<String> list = new ArrayList<>();
	
	public void add(String string){
		list.add(string);
	}
	
	public List<String> parse(){
		List<String> colored = new ArrayList<>();
		
		for (String string : list){
			colored.add(ChatColor.translateAlternateColorCodes('&', string));
		}
		
		return colored;
	}

}
