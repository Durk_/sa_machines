package me.zyrkran.machines;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.zyrkran.machines.commands.MachinesCommand;
import me.zyrkran.machines.listeners.MachineListener;
import me.zyrkran.machines.listeners.PlayerListener;
import me.zyrkran.machines.machines.MachineRegistry;
import me.zyrkran.machines.machines.objects.LivingMachine;

public class Main extends JavaPlugin{
	
	private static Main instance;
	
	public void onEnable(){
		instance = this;
		
		// load config 
		getConfig().options().copyDefaults(true);
		saveConfig();
		
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new MachineListener(), this);
		pm.registerEvents(new PlayerListener(), this);
		
		getCommand("machines").setExecutor(new MachinesCommand());
		
		MachineRegistry.registerMachines();
		MachineRegistry.loadMachines();
	}
	
	public void onDisable(){
		for (Player player : MachineRegistry.interaction_sessions.keySet()){
			player.closeInventory();
		}
		
		for (LivingMachine machine : MachineRegistry.getLivingMachines()){
			MachineRegistry.saveMachineData(machine);
		}
	}
	
	public static Main getInstance(){
		return instance;
	}

}
