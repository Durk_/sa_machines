package me.zyrkran.machines.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.zyrkran.machines.machines.objects.LivingMachine;

public class PlayerMachineInteractionEvent extends Event implements Cancellable{
	
	private Player player;
	private LivingMachine machine;
	
	private boolean isCancelled;
	
	public PlayerMachineInteractionEvent(Player player, LivingMachine machine){
		this.player = player;
		this.machine = machine;
	}
	
	public Player getPlayer(){
		return player;
	}
	
	public LivingMachine getMachine(){
		return machine;
	}
	
	private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

	@Override
	public boolean isCancelled() {
		return isCancelled;
	}

	@Override
	public void setCancelled(boolean bool) {
		this.isCancelled = bool;
	}

}
