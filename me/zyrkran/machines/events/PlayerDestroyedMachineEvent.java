package me.zyrkran.machines.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.zyrkran.machines.machines.objects.LivingMachine;

public class PlayerDestroyedMachineEvent extends Event {
	
	private Player player;
	private LivingMachine machine;
	
	public PlayerDestroyedMachineEvent(Player player, LivingMachine machine){
		this.player = player;
		this.machine = machine;
	}
	
	public Player getPlayer(){
		return player;
	}
	
	public LivingMachine getMachine(){
		return machine;
	}
	
	private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
