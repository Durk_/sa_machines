package me.zyrkran.machines.listeners;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Furnace;
import org.bukkit.material.Observer;

import me.zyrkran.machines.events.PlayerDestroyedMachineEvent;
import me.zyrkran.machines.events.PlayerMachineInteractionEvent;
import me.zyrkran.machines.events.PlayerPlacedMachineEvent;
import me.zyrkran.machines.machines.MachineRegistry;
import me.zyrkran.machines.machines.objects.LivingMachine;
import me.zyrkran.machines.machines.objects.LivingMachine.MachineStatus;
import me.zyrkran.machines.machines.objects.MachineCoalGenerator;
import me.zyrkran.machines.machines.objects.MachineCrusher;
import me.zyrkran.machines.machines.objects.MachineLavaGenerator;
import me.zyrkran.machines.machines.objects.MachineWaterGenerator;

public class PlayerListener implements Listener {
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event){
		Player player = (Player)event.getWhoClicked();
		
		if (event.getCurrentItem() == null){
			return;
		}
		
		if (MachineRegistry.interaction_sessions.containsKey(player)){
			LivingMachine machine = MachineRegistry.interaction_sessions.get(player);
			
			if (machine instanceof MachineCoalGenerator){
				if (event.getClickedInventory().equals(machine.getInventory())){
					if (event.isShiftClick() && event.getCurrentItem().getType() != Material.COAL){
						event.setCancelled(true);
						return;
					}
					
					MachineCoalGenerator generator = (MachineCoalGenerator) machine;
					
					if (event.getSlot() == 26){
						event.setCancelled(true);
						machine.toggleStatus();
						
						player.sendMessage("Generator is now turned " + machine.getStatus());
					}
					
					else if (event.getSlot() == 20){
						if (event.getCurrentItem().getType() != Material.COAL){							
							event.setCancelled(true);
							return; 
						}
						
						if (event.getClick().isLeftClick()){
							generator.setFuel(0);
						}				
					}
					
					else {
						if (event.getClickedInventory().equals(player.getInventory())){
							return;
						}
						
						event.setCancelled(true);
					}
				}
			}
			
			else if (machine instanceof MachineWaterGenerator){
				if (event.getClickedInventory().equals(machine.getInventory())){
					
					if (event.isShiftClick() && event.getCurrentItem().getType() != Material.WATER_BUCKET){
						event.setCancelled(true);
						return;
					}
					
					MachineWaterGenerator generator = (MachineWaterGenerator) machine;
					
					if (event.getSlot() == 26){
						event.setCancelled(true);
						generator.toggleStatus();
						
						player.sendMessage("Generator is now turned " + machine.getStatus());
					}
					
					else if (event.getSlot() == 20){
						event.setCancelled(true); // stop from picking up buckets
						
						if (event.getCursor().getType() == Material.WATER_BUCKET){	
							if (generator.getFuel() >= generator.getMaxFuel()){
								return;
							}
							
							event.getCursor().setAmount(event.getCursor().getAmount()-1);
							generator.addFuel(10);
						}
					}
					
					else {
						if (event.getClickedInventory().equals(player.getInventory())){
							return;
						}
						
						event.setCancelled(true);
					}
				}
			}
			
			else if (machine instanceof MachineLavaGenerator){
				if (event.getClickedInventory().equals(machine.getInventory())){
					
					if (event.isShiftClick() && event.getCurrentItem().getType() != Material.LAVA_BUCKET){
						event.setCancelled(true);
						return;
					}
					
					MachineLavaGenerator generator = (MachineLavaGenerator) machine;
					
					if (event.getSlot() == 26){
						event.setCancelled(true);
						generator.toggleStatus();
						
						player.sendMessage("Generator is now turned " + machine.getStatus());
					}
					
					else if (event.getSlot() == 20){
						event.setCancelled(true); // stop from picking up buckets
						
						if (event.getCursor().getType() == Material.LAVA_BUCKET){	
							if (generator.getFuel() >= generator.getMaxFuel()){
								return;
							}
							
							event.getCursor().setAmount(event.getCursor().getAmount()-1);
							generator.addFuel(10);
						}
					}
					
					else {
						if (event.getClickedInventory().equals(player.getInventory())){
							return;
						}
						
						event.setCancelled(true);
					}
				}
			}
			
			else if (machine instanceof MachineCrusher){
				if (event.getClickedInventory().equals(machine.getInventory())){
					List<Material> whitelist = Arrays.asList(Material.COAL, Material.IRON_INGOT, Material.GOLD_INGOT, Material.DIAMOND, Material.EMERALD);
					
					// if user clicks his inventory, ignore
					if (event.getClickedInventory().equals(player.getInventory())){
						if (event.isShiftClick() && !whitelist.contains(event.getCurrentItem().getType())){
							event.setCancelled(true);
						}
						return;
					}

					MachineCrusher crusher = (MachineCrusher) machine;
					
					if (event.getSlot() == 20){	
						
						// player tries to put items?
						if (!event.getCursor().equals(null) && event.getCurrentItem().getType().equals(Material.AIR)){
							if (!whitelist.contains(event.getCursor().getType()) || !event.isLeftClick()){
								event.setCancelled(true);
								return;
							}

							crusher.setInput(event.getCursor().getAmount());
							crusher.setInputMaterial(event.getCursor().getType());
							
						}
						
						// player tries to take items?
						else if (event.getCursor().getType().equals(Material.AIR)){
							if (!event.isLeftClick()){
								event.setCancelled(true);
								return;
							}
							
							crusher.setInput(0);
							crusher.setInputMaterial(Material.AIR);
						}
						
						return;
					}
					
					else if (event.getSlot() == 23){
//						if (event.getCursor() != null){
//							event.setCancelled(true);
//							return;
//						}
					}
					
					else if (event.getSlot() == 26){
						event.setCancelled(true); // stop players from taking items out of the inventory
						crusher.toggleStatus();
						
						player.sendMessage("Generator is now turned " + machine.getStatus());
						return;
					}
					
					else {
						event.setCancelled(true);
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event){
		if (!(event.getPlayer() instanceof Player)){
			return;
		}
		
		Player player = (Player) event.getPlayer();
		
		if (MachineRegistry.interaction_sessions.containsKey(player)){
			LivingMachine machine = MachineRegistry.interaction_sessions.get(player);
			
			if (event.getInventory().equals(machine.getInventory())){
				MachineRegistry.removeInteractionSession(player);
			}
		}
	}
	
	@EventHandler
	public void onBlockInteraction(PlayerInteractEvent event){
		Player player = event.getPlayer();
		
		if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
			
			LivingMachine machine = MachineRegistry.fromLocation(event.getClickedBlock().getLocation());
			if (machine == null){
				return;
			}
			
			event.setCancelled(true); // cancel default event
			
			// call interaction event
			Bukkit.getPluginManager().callEvent(new PlayerMachineInteractionEvent(player, machine));
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event){
		Player player = event.getPlayer();
		Block block   = event.getBlock();

		ItemStack item = player.getInventory().getItemInMainHand();
		String id =  ChatColor.stripColor(item.getItemMeta().getLore().get(0).replaceAll("Machine: ", ""));
		
		Date date = Calendar.getInstance().getTime();
		SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yyyy@hh:mm");
		
		// machine object
		LivingMachine machine = null;

		if (id.equalsIgnoreCase("coal_generator")){
			
			Furnace furnace = new Furnace(0, block.getData()); // this gives the ability to check for direction
			machine = new MachineCoalGenerator(
					MachineRegistry.getByName(id),
					MachineStatus.OFF, 
					0, 
					0, 
					0, 
					format.format(date), 
					UUID.randomUUID(), 
					block.getLocation(), 
					furnace.getFacing(), 
					player.getUniqueId());
			((MachineCoalGenerator)machine).setBlockFace(furnace.getFacing());
		}
		
		else if (id.equalsIgnoreCase("water_generator")){
			Furnace furnace = new Furnace(0, block.getData()); // this gives the ability to check for direction
			machine = new MachineWaterGenerator(
					MachineRegistry.getByName(id),
					MachineStatus.OFF,
					0,
					0,
					0,
					UUID.randomUUID(), 
					block.getLocation(), 
					furnace.getFacing(),
					player.getUniqueId(), 
					format.format(date));
			((MachineWaterGenerator)machine).setBlockFace(furnace.getFacing());
		}
		
		else if (id.equalsIgnoreCase("lava_generator")){
			Furnace furnace = new Furnace(0, block.getData()); // this gives the ability to check for direction
			machine = new MachineLavaGenerator(
					MachineRegistry.getByName(id),
					MachineStatus.OFF,
					0,
					0,
					0,
					UUID.randomUUID(), 
					block.getLocation(), 
					furnace.getFacing(),
					player.getUniqueId(), 
					format.format(date));
			((MachineLavaGenerator)machine).setBlockFace(furnace.getFacing());
		}
		
		else if (id.equalsIgnoreCase("crusher")){
			
			Observer observer = new Observer(0, block.getData()); // this gives the ability to check for direction
			machine = new MachineCrusher(
					MachineRegistry.getByName(id),
					0,
					Material.AIR,
					0,
					Material.AIR,
					UUID.randomUUID(), 
					block.getLocation(),
					player.getUniqueId(), 
					format.format(date));
			((MachineCrusher)machine).setBlockFace(observer.getFacing());
		}

		// call custom event
		PlayerPlacedMachineEvent e = new PlayerPlacedMachineEvent(player, machine);
		Bukkit.getPluginManager().callEvent(e);
	}
	
	
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event){
		Player player = event.getPlayer();
		Block block   = event.getBlock();
		
		LivingMachine machine = MachineRegistry.fromLocation(block.getLocation());
		if (machine == null){
			return;
		}
		
		PlayerDestroyedMachineEvent e = new PlayerDestroyedMachineEvent(player, machine);
		
		Bukkit.getPluginManager().callEvent(e);
		
	}
}
