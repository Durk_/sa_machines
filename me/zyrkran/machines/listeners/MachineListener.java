package me.zyrkran.machines.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import me.zyrkran.machines.events.PlayerDestroyedMachineEvent;
import me.zyrkran.machines.events.PlayerMachineInteractionEvent;
import me.zyrkran.machines.events.PlayerPlacedMachineEvent;
import me.zyrkran.machines.machines.Destroyable;
import me.zyrkran.machines.machines.MachineRegistry;
import me.zyrkran.machines.machines.objects.LivingMachine;
import net.md_5.bungee.api.ChatColor;

public class MachineListener implements Listener{
	
	@EventHandler
	public void onMachineInteract(PlayerMachineInteractionEvent event){
		Player player = event.getPlayer();
		LivingMachine machine = event.getMachine();
		
		if (!MachineRegistry.isBeingUsed(machine)){
			machine.openMachineInventory(player);
			MachineRegistry.addInteractionSession(player, machine);
			return;
		}

		else {
			event.setCancelled(true);
			player.sendMessage(ChatColor.RED + "Someone is already using this machine!");
		}
	}
	
	@EventHandler
	public void onMachinePlace(PlayerPlacedMachineEvent event){
		Player player = event.getPlayer();
		LivingMachine machine = event.getMachine();
		
		MachineRegistry.addMachine(machine);
		player.sendMessage("Placed a " + machine.getID() + "(" + machine.getType() + ")" + " machine!");
	}
	
	@EventHandler
	public void onMachineDestroy(PlayerDestroyedMachineEvent event){
		Player player = event.getPlayer();
		LivingMachine machine = event.getMachine();
		
		// living machine is removed
		if (machine instanceof Destroyable){
			((Destroyable)machine).destroy();
		}
		
		MachineRegistry.removeMachine(machine.getUniqueId());
		player.sendMessage("Destroyed a " + machine.getID() + "(" + machine.getType() + ")" + " machine!");
				
	}
	

}
