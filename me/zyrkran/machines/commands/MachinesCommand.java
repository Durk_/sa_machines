package me.zyrkran.machines.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.zyrkran.machines.machines.MachineRegistry;
import me.zyrkran.machines.machines.objects.MachineItemStack;
import me.zyrkran.machines.machines.objects.MachineObject;
import net.md_5.bungee.api.ChatColor;

public class MachinesCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (!(sender instanceof Player)){
			return false;
		}
		
		Player player = (Player) sender;
		
		if (args.length == 0){
			// send help guide
			return true;
		}

		// machines give zyrkran machine
		else if (args[0].equalsIgnoreCase("give")){
			if (args.length == 3){
				
				Player target = Bukkit.getPlayer(args[1]);
				if (target == null){
					player.sendMessage(ChatColor.RED + "Couldn't find player!");
					return false;
				}
				
				MachineObject obj = MachineRegistry.getByName(args[2]);
				if (obj == null){
					player.sendMessage(ChatColor.RED + "Invalid Machine type!");
					return false;
				}
				
				player.getInventory().addItem(new MachineItemStack(obj).create());
				player.sendMessage(ChatColor.GOLD + "Received a " + obj.getID() + " machine in your inventory!");
				return true;
				
			}
			
			else {
				player.sendMessage(ChatColor.RED  + "Usage: /machines give <player> <machine>");
				return false;
			}
		}
		
		return false;
	}

}
