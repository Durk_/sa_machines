package me.zyrkran.machines.machines;

public interface Destroyable {
	
	public void destroy();

}
