package me.zyrkran.machines.machines;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import me.zyrkran.machines.Main;
import me.zyrkran.machines.machines.objects.LivingMachine;
import me.zyrkran.machines.machines.objects.LivingMachine.MachineStatus;
import me.zyrkran.machines.machines.objects.MachineCoalGenerator;
import me.zyrkran.machines.machines.objects.MachineCrusher;
import me.zyrkran.machines.machines.objects.MachineLavaGenerator;
import me.zyrkran.machines.machines.objects.MachineObject;
import me.zyrkran.machines.machines.objects.MachineWaterGenerator;
import me.zyrkran.machines.parser.LocationParser;

public class MachineRegistry {

	public enum MachineType {
		MACHINE, GENERATOR
	}
	
	private static final List<MachineObject> machines = new ArrayList<>();
	private static List<LivingMachine> livingMachines = new ArrayList<>();
	
	public static HashMap<Player, LivingMachine> interaction_sessions = new HashMap<>();
	
	public static void registerMachines(){
		machines.add(new MachineObject("coal_generator", Material.FURNACE, MachineType.GENERATOR));
		machines.add(new MachineObject("water_generator", Material.FURNACE, MachineType.GENERATOR));
		machines.add(new MachineObject("lava_generator", Material.FURNACE, MachineType.GENERATOR));
		machines.add(new MachineObject("crusher", Material.OBSERVER, MachineType.MACHINE));
	}
	
	public static void loadMachines(){
		if (Main.getInstance().getConfig().getConfigurationSection("machines") == null){
			return;
		}
		
		for (String uuid : Main.getInstance().getConfig().getConfigurationSection("machines").getKeys(false)){
			
			LivingMachine machine = null; 
			String id = Main.getInstance().getConfig().getString("machines." + uuid + ".machine-id");
			
			if (id.equalsIgnoreCase("coal_generator")){
				String location = Main.getInstance().getConfig().getString("machines." + uuid + ".location");
				String owner = Main.getInstance().getConfig().getString("machines." + uuid + ".owner");
				String status = Main.getInstance().getConfig().getString("machines." + uuid + ".machine-status");
				String lastpower = Main.getInstance().getConfig().getString("machines." + uuid + ".last-power");
				String lastfuel = Main.getInstance().getConfig().getString("machines." + uuid + ".last-fuel");
				String totalpower = Main.getInstance().getConfig().getString("machines." + uuid + ".total-power");
				String blockface = Main.getInstance().getConfig().getString("machines." + uuid + ".blockface");
				String date = Main.getInstance().getConfig().getString("machines." + uuid + ".date");
				
				// create machine object
				machine = new MachineCoalGenerator(
						getByName(id), 
						MachineStatus.valueOf(status), 
						Integer.parseInt(lastpower), 
						Integer.parseInt(lastfuel), 
						Integer.parseInt(totalpower), 
						date, 
						UUID.fromString(uuid), 
						new LocationParser().parseString(location), 
						BlockFace.valueOf(blockface), 
						UUID.fromString(owner));
				
			}
			
			else if (id.equalsIgnoreCase("crusher")){
				String location = Main.getInstance().getConfig().getString("machines." + uuid + ".location");
				String owner = Main.getInstance().getConfig().getString("machines." + uuid + ".owner");
				String status = Main.getInstance().getConfig().getString("machines." + uuid + ".machine-status");
				String blockface = Main.getInstance().getConfig().getString("machines." + uuid + ".blockface");
				String date = Main.getInstance().getConfig().getString("machines." + uuid + ".date");
				String input_amount = Main.getInstance().getConfig().getString("machines." + uuid + ".input-amount");
				String input_material = Main.getInstance().getConfig().getString("machines." + uuid + ".input-material");
				String output_amount = Main.getInstance().getConfig().getString("machines." + uuid + ".output-amount");
				String output_material = Main.getInstance().getConfig().getString("machines." + uuid + ".output-material");
				
				// create machine object
				machine = new MachineCrusher(
						getByName(id),
						Integer.valueOf(input_amount),
						Material.valueOf(input_material),
						Integer.valueOf(output_amount),
						Material.valueOf(output_material),
						UUID.fromString(uuid),
						new LocationParser().parseString(location), 
						UUID.fromString(owner),
						date);
				
				((MachineCrusher)machine).setStatus(MachineStatus.valueOf(status));
				((MachineCrusher)machine).setBlockFace(BlockFace.valueOf(blockface));
				
			}
			
			else if (id.equalsIgnoreCase("water_generator")){
				String location = Main.getInstance().getConfig().getString("machines." + uuid + ".location");
				String owner = Main.getInstance().getConfig().getString("machines." + uuid + ".owner");
				String status = Main.getInstance().getConfig().getString("machines." + uuid + ".machine-status");
				String lastpower = Main.getInstance().getConfig().getString("machines." + uuid + ".last-power");
				String lastfuel = Main.getInstance().getConfig().getString("machines." + uuid + ".last-fuel");
				String totalpower = Main.getInstance().getConfig().getString("machines." + uuid + ".total-power");
				String blockface = Main.getInstance().getConfig().getString("machines." + uuid + ".blockface");
				String date = Main.getInstance().getConfig().getString("machines." + uuid + ".date");
				
				// create machine object
				machine = new MachineWaterGenerator(
						getByName(id), 
						MachineStatus.valueOf(status), 
						Integer.parseInt(lastpower), 
						Integer.parseInt(lastfuel), 
						Integer.parseInt(totalpower),
						UUID.fromString(uuid), 
						new LocationParser().parseString(location), 
						BlockFace.valueOf(blockface), 
						UUID.fromString(owner), 
						date);
			}
			
			else if (id.equalsIgnoreCase("lava_generator")){
				String location = Main.getInstance().getConfig().getString("machines." + uuid + ".location");
				String owner = Main.getInstance().getConfig().getString("machines." + uuid + ".owner");
				String status = Main.getInstance().getConfig().getString("machines." + uuid + ".machine-status");
				String lastpower = Main.getInstance().getConfig().getString("machines." + uuid + ".last-power");
				String lastfuel = Main.getInstance().getConfig().getString("machines." + uuid + ".last-fuel");
				String totalpower = Main.getInstance().getConfig().getString("machines." + uuid + ".total-power");
				String blockface = Main.getInstance().getConfig().getString("machines." + uuid + ".blockface");
				String date = Main.getInstance().getConfig().getString("machines." + uuid + ".date");
				
				// create machine object
				machine = new MachineLavaGenerator(
						getByName(id), 
						MachineStatus.valueOf(status), 
						Integer.parseInt(lastpower), 
						Integer.parseInt(lastfuel), 
						Integer.parseInt(totalpower),
						UUID.fromString(uuid), 
						new LocationParser().parseString(location), 
						BlockFace.valueOf(blockface), 
						UUID.fromString(owner), 
						date);
			}
			
			// finally, add machine to memory
			livingMachines.add(machine);
		}
	}
	
	public static boolean isBeingUsed(LivingMachine machine){
		boolean found = false;
		for (LivingMachine m : interaction_sessions.values()){
			if (m.equals(machine)){
				found = true;
			}
		}
		
		return found;
	}

	public static void addInteractionSession(Player player, LivingMachine machine){	
		interaction_sessions.put(player, machine);
	}
	
	public static void removeInteractionSession(Player player){	
		interaction_sessions.remove(player);
	}
	
	public static void saveMachineData(LivingMachine machine){
		if (machine instanceof MachineCoalGenerator){
			MachineCoalGenerator generator = (MachineCoalGenerator)machine;
			
			// save data to config
			String location = new LocationParser(generator.getLocation()).parse();
			String uuid = generator.getUniqueId().toString();

			Main.getInstance().getConfig().set("machines." + uuid + ".machine-id", generator.getID());
			Main.getInstance().getConfig().set("machines." + uuid + ".machine-type", generator.getType().name());
			Main.getInstance().getConfig().set("machines." + uuid + ".machine-status", generator.getStatus().name());
			Main.getInstance().getConfig().set("machines." + uuid + ".location", location);
			Main.getInstance().getConfig().set("machines." + uuid + ".owner", generator.getOwner().toString());
			Main.getInstance().getConfig().set("machines." + uuid + ".last-power", ((MachineCoalGenerator) machine).getCurrentPower());
			Main.getInstance().getConfig().set("machines." + uuid + ".last-fuel", generator.getFuel());
			Main.getInstance().getConfig().set("machines." + uuid + ".total-power", generator.getTotalPower());
			Main.getInstance().getConfig().set("machines." + uuid + ".blockface", generator.getBlockFace().name());
			Main.getInstance().getConfig().set("machines." + uuid + ".date", generator.getDate());
			
			Main.getInstance().saveConfig();
		}
		
		else if (machine instanceof MachineWaterGenerator){
			MachineWaterGenerator generator = (MachineWaterGenerator)machine;
			
			// save data to config
			String location = new LocationParser(generator.getLocation()).parse();
			String uuid = generator.getUniqueId().toString();

			Main.getInstance().getConfig().set("machines." + uuid + ".machine-id", generator.getID());
			Main.getInstance().getConfig().set("machines." + uuid + ".machine-type", generator.getType().name());
			Main.getInstance().getConfig().set("machines." + uuid + ".machine-status", generator.getStatus().name());
			Main.getInstance().getConfig().set("machines." + uuid + ".location", location);
			Main.getInstance().getConfig().set("machines." + uuid + ".owner", generator.getOwner().toString());
			Main.getInstance().getConfig().set("machines." + uuid + ".last-power", ((MachineWaterGenerator) machine).getPower());
			Main.getInstance().getConfig().set("machines." + uuid + ".last-fuel", generator.getFuel());
			Main.getInstance().getConfig().set("machines." + uuid + ".total-power", generator.getTotalPower());
			Main.getInstance().getConfig().set("machines." + uuid + ".blockface", generator.getBlockFace().name());
			Main.getInstance().getConfig().set("machines." + uuid + ".date", generator.getDate());
			
			Main.getInstance().saveConfig();
		}
		
		else if (machine instanceof MachineLavaGenerator){
			MachineLavaGenerator generator = (MachineLavaGenerator)machine;
			
			// save data to config
			String location = new LocationParser(generator.getLocation()).parse();
			String uuid = generator.getUniqueId().toString();

			Main.getInstance().getConfig().set("machines." + uuid + ".machine-id", generator.getID());
			Main.getInstance().getConfig().set("machines." + uuid + ".machine-type", generator.getType().name());
			Main.getInstance().getConfig().set("machines." + uuid + ".machine-status", generator.getStatus().name());
			Main.getInstance().getConfig().set("machines." + uuid + ".location", location);
			Main.getInstance().getConfig().set("machines." + uuid + ".owner", generator.getOwner().toString());
			Main.getInstance().getConfig().set("machines." + uuid + ".last-power", ((MachineLavaGenerator) machine).getPower());
			Main.getInstance().getConfig().set("machines." + uuid + ".last-fuel", generator.getFuel());
			Main.getInstance().getConfig().set("machines." + uuid + ".total-power", generator.getTotalPower());
			Main.getInstance().getConfig().set("machines." + uuid + ".blockface", generator.getBlockFace().name());
			Main.getInstance().getConfig().set("machines." + uuid + ".date", generator.getDate());
			
			Main.getInstance().saveConfig();
		}
		
		else if (machine instanceof MachineCrusher){
			MachineCrusher crusher = (MachineCrusher)machine;
			
			String location = new LocationParser(crusher.getLocation()).parse();
			String uuid = crusher.getUniqueId().toString();
			
			Main.getInstance().getConfig().set("machines." + uuid + ".machine-id", crusher.getID());
			Main.getInstance().getConfig().set("machines." + uuid + ".machine-type", crusher.getType().name());
			Main.getInstance().getConfig().set("machines." + uuid + ".machine-status", crusher.getStatus().name());
			Main.getInstance().getConfig().set("machines." + uuid + ".location", location);
			Main.getInstance().getConfig().set("machines." + uuid + ".owner", crusher.getOwner().toString());
			Main.getInstance().getConfig().set("machines." + uuid + ".blockface", crusher.getBlockFace().name());
			Main.getInstance().getConfig().set("machines." + uuid + ".date", crusher.getDate());
			Main.getInstance().getConfig().set("machines." + uuid + ".input-amount", crusher.getInput());
			Main.getInstance().getConfig().set("machines." + uuid + ".input-material", crusher.getInputMaterial().name());
			Main.getInstance().getConfig().set("machines." + uuid + ".output-amount", crusher.getOutput());
			Main.getInstance().getConfig().set("machines." + uuid + ".output-material", crusher.getOutputMaterial().name());
			
			Main.getInstance().saveConfig();
		}
		
	}
	
	public static void addMachine(LivingMachine machine){
		livingMachines.add(machine);
		saveMachineData(machine);
	}
	
	public static void removeMachine(UUID uuid){
		for (LivingMachine machine : livingMachines){
			if (machine.getUniqueId().equals(uuid)){
				livingMachines.remove(machine);
				
				// handle config file
				Main.getInstance().getConfig().set("machines." + machine.getUniqueId(), null);
				Main.getInstance().saveConfig();
				break;
			}
		}		
	}
	
	public static List<MachineObject> getRefMachines(){
		return machines;
	}
	
	public static List<LivingMachine> getLivingMachines(){
		return livingMachines;
	}
	
	public static LivingMachine fromLocation(Location location){
		for (LivingMachine machine : livingMachines){
			if (machine.getLocation().equals(location)){
				return machine;
			}
		}
		return null;
	}
	
	public static MachineObject getByName(String name){
		for (MachineObject obj : machines){
			if (obj.getID().equalsIgnoreCase(name)){
				return obj;
			}
		}
		return null;
	}

}
