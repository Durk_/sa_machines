package me.zyrkran.machines.machines;

import org.bukkit.inventory.Inventory;

public interface MachineInteraction {
	
	public void setupMenu();
	
	public Inventory updateInventory();

}
