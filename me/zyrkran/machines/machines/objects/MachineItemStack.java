package me.zyrkran.machines.machines.objects;

import java.util.Arrays;

import org.apache.commons.lang.WordUtils;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class MachineItemStack {
	
	private ItemStack item;
	
	public MachineItemStack(MachineObject object){
		item = new ItemStack(object.getMaterial());
		ItemMeta meta = item.getItemMeta();
		
		String displayname = WordUtils.capitalizeFully(object.getID().replaceAll("_", " "));
		meta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + displayname);
		meta.setLore(Arrays.asList("Machine: " + object.getID()));
		item.setItemMeta(meta);
	}
	
	public ItemStack create(){
		return item;
	}

}
