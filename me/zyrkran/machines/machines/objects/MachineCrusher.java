package me.zyrkran.machines.machines.objects;

import java.util.UUID;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import me.zyrkran.machines.Main;
import me.zyrkran.machines.machines.MachineInteraction;
import me.zyrkran.machines.parser.Lore;
import net.md_5.bungee.api.ChatColor;

public class MachineCrusher extends LivingMachine implements MachineInteraction{
	
	private Material INPUT_MAT = null;
	private Material OUTPUT_MAT = null;
	
	private int INPUT = 0;
	private int OUTPUT = 0;
	
	private boolean crushing = false;
	private BlockFace blockface = null;
	
	private BukkitRunnable runnable;

	public MachineCrusher(MachineObject object, int input_amount, Material input_material, int output_amount, Material output_material, UUID uuid, Location location, UUID owner, String date) {
		super(object, "Crusher", uuid, location, owner, date);
		
		this.INPUT = input_amount;
		this.INPUT_MAT = input_material;
		this.OUTPUT = output_amount;
		this.OUTPUT_MAT = output_material;
		
		setupMenu();
		setStatus(MachineStatus.OFF);
		
		// main loop
		runnable = new BukkitRunnable() {
			
			@Override
			public void run() {
				if (getStatus() == MachineStatus.ON){
					if (!(OUTPUT > 64 && INPUT < 0)){
						// if there's no item at slot '20' the machine should stop
						if (getInventory().getItem(20) == null){
							setInput(0);
							setInputMaterial(Material.AIR);
							return;
						}
						
						else {
							if (getInventory().getItem(23) == null){
								setOutput(0);
								setOutputMaterial(Material.AIR);
							}
							
							setInput(getInput() - 1);
							setInputMaterial(getInputMaterial());

							setOutput(getOutput() + 1);
							setOutputMaterial(getInputMaterial());
							
						}
						
					}

				}
				
				else {
					// if there's no item at slot '20' the machine should stop
					if (getInventory().getItem(20) == null){
						setInput(0);
						setInputMaterial(Material.AIR);
						return;
					}
					
					ItemStack item = getInventory().getItem(20);
					
					setInput(item.getAmount());
					setInputMaterial(item.getType());		

					if (getInventory().getItem(23) == null){
						setOutput(0);
						setOutputMaterial(Material.AIR);
						return;
					}
					
					setOutput(getInventory().getItem(23).getAmount());
					setOutputMaterial(getInventory().getItem(23).getType());
				}
				
				updateInventory();
				
			}
		};
		
		runnable.runTaskTimer(Main.getInstance(), 0, 20);
	}
	
	public void setInputMaterial(Material material){
		this.INPUT_MAT = material;
	}
	
	public Material getInputMaterial(){
		return INPUT_MAT == null ? Material.AIR : INPUT_MAT;
	}
	
	public void setInput(int i){
		this.INPUT = i;
	}
	
	public int getInput(){
		return INPUT;
	}
	
	public void setOutput(int i){
		this.OUTPUT = i;
	}
	
	public int getOutput(){
		return OUTPUT;
	}
	
	public void setCrushing(boolean b){
		this.crushing = b;
	}
	
	public void setOutputMaterial(Material material){
		this.OUTPUT_MAT = material;
	}
	
	public Material getOutputMaterial(){
		return OUTPUT_MAT == null ? Material.AIR : OUTPUT_MAT;
	}
	
	public boolean isCrushing(){
		return crushing;
	}
	
	public void setBlockFace(BlockFace blockface){
		this.blockface = blockface;
	}
	
	public BlockFace getBlockFace(){
		return blockface;
	}

	@Override
	public void setupMenu() {
		
		// generate basic layout
		for (int i=0; i<45; i++){
			ItemStack item = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)15);
			ItemMeta meta = item.getItemMeta();
			
			meta.setDisplayName(" ");
			item.setItemMeta(meta);
			getInventory().setItem(i, item);
		}
		
		// generate white frame
		int[] white = {10,11,12,13,14,15,19,21,22,24,28,29,30,31,32,33};
		for (Integer i : white){
			ItemStack item = new ItemStack(Material.STAINED_GLASS_PANE, 1);
			ItemMeta meta = item.getItemMeta();
			
			meta.setDisplayName(" ");
			item.setItemMeta(meta);
			getInventory().setItem(i, item);
		}

		// update dynamic items
		updateInventory();
	}

	@Override
	public Inventory updateInventory() {	
		
		ItemStack item = null;
		ItemMeta meta  = null;
		
		// input slot
		item = new ItemStack(INPUT_MAT, INPUT);
		getInventory().setItem(20, item);
		
		// output slot
		item = new ItemStack(OUTPUT_MAT, OUTPUT);
		if (OUTPUT_MAT != Material.AIR){
			meta = item.getItemMeta();
			meta.setDisplayName("Crushed " + WordUtils.capitalizeFully(item.getType().name().replace("_", " ")));
			item.setItemMeta(meta);
		}
		
		getInventory().setItem(23, item);
		
		// stats slot
		item = new ItemStack(Material.SIGN);
		meta = item.getItemMeta();
		
		Lore lore = new Lore();
		lore.add("&6» &fOwner: &b" + Bukkit.getOfflinePlayer(getOwner()).getName());
		lore.add("&6» &fDate created: &b" + getDate());
		lore.add("&6» &fSharing mode: &b{PUBLIC or PRIVATE}");

		meta.setDisplayName(ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Power statistics");
		meta.setLore(lore.parse());
		item.setItemMeta(meta);
		
		getInventory().setItem(17, item);
		
		// extra slot
		item = new ItemStack(Material.CHEST);
		meta = item.getItemMeta(); 
		meta.setDisplayName(ChatColor.RED + "Coming soon");
		item.setItemMeta(meta);
		getInventory().setItem(35, item);
		
		return getInventory();
	}
}
