package me.zyrkran.machines.machines.objects;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import me.zyrkran.machines.Main;
import me.zyrkran.machines.machines.Destroyable;
import me.zyrkran.machines.machines.MachineInteraction;
import me.zyrkran.machines.parser.Lore;
import net.md_5.bungee.api.ChatColor;

public class MachineLavaGenerator extends LivingMachine implements MachineInteraction, Destroyable{
	
	private int FUEL  = 0;
	private int MAX_FUEL = 100;
	private int POWER = 0;
	private int MAX_POWER = 100;
	private int TOTAL_POWER = 0;
	
	private boolean burning = false;
	private BlockFace blockFace;

	private BukkitRunnable runnable;
	private BukkitRunnable burningEffect;

	public MachineLavaGenerator(MachineObject object, MachineStatus status, int lastpower, int lastfuel, int totalpower, UUID uuid, Location location, BlockFace blockface, UUID owner, String date) {
		super(object, "Lava Generator", uuid, location, owner, date);
		
		FUEL = lastfuel;
		POWER = lastpower;
		TOTAL_POWER = totalpower;
		blockFace = blockface;
		
		setupMenu();
		setStatus(status);
		
		runnable = new BukkitRunnable() {
			
			@Override
			public void run() {
				
				if (getStatus() == MachineStatus.ON){
					if (FUEL > 0 && !(POWER > MAX_POWER)){
						POWER++;
						TOTAL_POWER++;
						FUEL--;
						
						burning = true;
					}
					
					else {
						burning = false;
					}
				}
				
				else {
					burning = false;
				}

				updateInventory();
			}
		};
		
		// Burning effect
		burningEffect = new BukkitRunnable() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				if (isBurning()){
					// burning effect on
					Block block = getLocation().getBlock();
					block.setType(Material.BURNING_FURNACE);
					block.setData(getDirectionData(blockFace), true);
					
				}
				
				else {
					Block block = getLocation().getBlock();
					block.getState().setType(Material.FURNACE);
					block.setData(getDirectionData(blockFace), true);
				}
			}
			
			private byte getDirectionData(BlockFace face){
				if (face.equals(BlockFace.NORTH)) return 2;
				if (face.equals(BlockFace.SOUTH)) return 3;
				if (face.equals(BlockFace.WEST)) return 4;
				if (face.equals(BlockFace.EAST)) return 5;
				return 0;
			}
			
		};
		
		runnable.runTaskTimer(Main.getInstance(), 0, 20);
		burningEffect.runTaskTimer(Main.getInstance(), 0, 1);
		
	}
	
	public boolean isBurning(){
		return burning;
	}
	
	public void addFuel(int i){
		if (FUEL + i > MAX_FUEL){
			FUEL = MAX_FUEL;
		}
		
		FUEL += i;
	}
	
	public int getFuel(){
		return FUEL;
	}
	
	public int getMaxFuel(){
		return MAX_FUEL;
	}
	
	public int getTotalPower(){
		return TOTAL_POWER;
	}
	
	public int getPower(){
		return POWER;
	}

	public void setBlockFace(BlockFace face){
		this.blockFace = face;
	}
	
	public BlockFace getBlockFace(){
		return blockFace;
	}

	@Override
	public void destroy(){
		runnable.cancel();
		runnable = null;
		
		burningEffect.cancel();
		burningEffect = null;
	}

	@Override
	public void setupMenu() {
		
		// generate basic layout
		for (int i=0; i<45; i++){
			ItemStack item = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)15);
			ItemMeta meta = item.getItemMeta();
			
			meta.setDisplayName(" ");
			item.setItemMeta(meta);
			getInventory().setItem(i, item);
		}
		
		// generate white frame
		int[] white = {10,11,12,13,14,15,19,21,22,24,28,29,30,31,32,33};
		for (Integer i : white){
			ItemStack item = new ItemStack(Material.STAINED_GLASS_PANE, 1);
			ItemMeta meta = item.getItemMeta();
			
			meta.setDisplayName(" ");
			item.setItemMeta(meta);
			getInventory().setItem(i, item);
		}
		
		updateInventory();
	}

	@Override
	public Inventory updateInventory() {	
		
		ItemStack item = null;
		ItemMeta meta  = null;
		
		// fuel slot
		item = new ItemStack(FUEL == 0 ? Material.BUCKET : Material.LAVA_BUCKET, 1);
		meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&a&nFuel&r&f: &7(" + FUEL + "/" + MAX_FUEL +")"));
		item.setItemMeta(meta);
		
		getInventory().setItem(20, item);
		
		// power slot
		item = new ItemStack(Material.BLAZE_POWDER);
		meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&a&nPower&r&f: &7(" + POWER + "/" + MAX_POWER +")"));
		item.setItemMeta(meta);
		
		getInventory().setItem(23, item);	
		
		// stats slot
		item = new ItemStack(Material.SIGN);
		meta = item.getItemMeta();
		
		Lore lore = new Lore();
		lore.add("&6» &fOwner: &b" + Bukkit.getOfflinePlayer(getOwner()).getName());
		lore.add("&6» &fDate created: &b" + getDate());
		lore.add("&6» &fSharing mode: &b{PUBLIC or PRIVATE}");
		lore.add("");
		lore.add("&6» &fTotal power generated: &b" + TOTAL_POWER);
		lore.add("&6» &fTotal power outputted: &bN/A");

		meta.setDisplayName(ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Power statistics");
		meta.setLore(lore.parse());
		item.setItemMeta(meta);
		
		getInventory().setItem(17, item);
		
		// extra slot
		item = new ItemStack(Material.CHEST);
		meta = item.getItemMeta(); 
		meta.setDisplayName(ChatColor.RED + "Coming soon");
		item.setItemMeta(meta);
		getInventory().setItem(35, item);
		
		return getInventory();
	}

}
