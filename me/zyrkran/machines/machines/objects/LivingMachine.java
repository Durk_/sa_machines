package me.zyrkran.machines.machines.objects;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.zyrkran.machines.parser.Lore;
import net.md_5.bungee.api.ChatColor;

public abstract class LivingMachine extends MachineObject {
	
	public enum MachineStatus {
		ON, OFF
	}
	
	private String date;
	private MachineStatus status;
	
	private UUID uuid, owner;
	private Location location;

	private Inventory inventory;
	
	public LivingMachine(MachineObject object, String title, UUID uuid, Location location, UUID owner, String date){
		super(object.getID(), object.getMaterial(), object.getType());
		
		this.uuid = uuid;
		this.location = location;
		this.owner = owner;
		this.date = date;
		
		this.inventory = Bukkit.createInventory(null, 45, title);
	}
	
	public void updatePowerButton(MachineStatus status){
		if (status == MachineStatus.ON){
			ItemStack item = new ItemStack(Material.INK_SACK, 1, (short)10);
			ItemMeta meta = item.getItemMeta();

			Lore lore = new Lore();
			lore.add("");
			lore.add("&fThis machine is currently enabled.");
			lore.add("");
			lore.add("&cClick to disable this machine.");
			
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&fStatus: &aENABLED"));
			meta.setLore(lore.parse());
			item.setItemMeta(meta);
			
			getInventory().setItem(26, item);	
		}
		
		else if (status == MachineStatus.OFF){
			ItemStack item = new ItemStack(Material.INK_SACK, 1, (short)8);
			ItemMeta meta = item.getItemMeta();

			Lore lore = new Lore();
			lore.add("");
			lore.add("&fThis machine is currently disabled.");
			lore.add("");
			lore.add("&aClick to enable this machine.");
			
			meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&fStatus: &cDISABLED"));
			meta.setLore(lore.parse());
			item.setItemMeta(meta);
			
			getInventory().setItem(26, item);
		}
	}
	
	public void openMachineInventory(Player player){
		player.openInventory(getInventory());
	}
	
	public void setStatus(MachineStatus status){
		this.status = status;
		updatePowerButton(status);
	}
	
	public void toggleStatus(){
		if (status == MachineStatus.ON){
			status = MachineStatus.OFF;
		}
		
		else {
			status = MachineStatus.ON;
		}
		
		updatePowerButton(status);
	}
	
	public MachineStatus getStatus(){
		return status;
	}
	
	public UUID getOwner(){
		return owner;
	}
	
	public UUID getUniqueId(){
		return uuid;
	}
	
	public void setUniqueId(UUID uuid){
		this.uuid = uuid;
	}
	
	public Location getLocation(){
		return location;
	}
	
	public Inventory getInventory(){
		return inventory;
	}
	
	public String getDate(){
		return date;
	}
}
