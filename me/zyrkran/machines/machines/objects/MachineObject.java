package me.zyrkran.machines.machines.objects;

import org.bukkit.Material;

import me.zyrkran.machines.machines.MachineRegistry.MachineType;

public class MachineObject {
	
	private String id;
	private Material material;
	private MachineType type;
	
	public MachineObject(String string, Material material, MachineType type){
		this.id = string;
		this.material = material;
		this.type = type;
	}
	
	public String getID(){
		return id;
	}
	
	public Material getMaterial(){
		return material;
	}
	
	public MachineType getType(){
		return type;
	}
	
}
